# Content-Type vs Accept

**Content-Type** describes type of request content.
**Accept** describes requested response content type.

# XML

In order to use xml:
* you need to product xml for given class/method
* you need to annotation serialized classes with JAXB annotations
* you need to ask for xml media type using Accept header attribute

# Json

Json response format is not available out of the box.

You need to add some dependencies:

```xml
<dependencies>
    <dependency>
        <groupId>org.codehaus.jackson</groupId>
        <artifactId>jackson-jaxrs</artifactId>
    </dependency>
    <dependency>
        <groupId>com.fasterxml.jackson.jaxrs</groupId>
        <artifactId>jackson-jaxrs-json-provider</artifactId>
    </dependency>    
</dependencies>
```

You need to configure json provider

```java
JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
sf.setResourceClasses(PersonResourceImpl.class);
sf.setResourceProvider(PersonResourceImpl.class, new SingletonResourceProvider(new PersonResourceImpl()));
sf.setAddress("http://localhost:8080/");
sf.setProvider(new JacksonJaxbJsonProvider()); // <<<<<<<<<<
sf.create();
``` 

The rest is similar to XML. Use @Produce with MediaType.APPLICATION_JSON and specify "application/json" in Accept header.

# You control which media types are produced using @Produces annotion.

You can set it either on class 

```java
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class Resources {
    
}
```

or on method

```java
public class Resources {
    @Path("/resource/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Entity get(@PathParam("id") String id) {
        
    }    
}
```