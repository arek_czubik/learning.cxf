package org.ack;

import org.ack.service.HelloWorld;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("configuration.xml");
        HelloWorld client = ctx.getBean("client", HelloWorld.class);
        System.out.println(client.sayHi("Arek"));
    }
}
