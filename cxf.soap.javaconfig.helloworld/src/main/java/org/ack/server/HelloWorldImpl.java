package org.ack.server;

import javax.jws.WebService;

@WebService(
        endpointInterface = "org.ack.server.HelloWorld",
        targetNamespace = "http://examples.czubik.org/",
        serviceName = "HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    @Override
    public String sayHi(String text) {
        return "Hi " + text;
    }
}
