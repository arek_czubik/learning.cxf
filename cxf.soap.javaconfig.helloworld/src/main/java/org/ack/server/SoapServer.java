package org.ack.server;

import javax.xml.ws.Endpoint;

public class SoapServer {
    public static void main(String[] args) {
        System.out.println("Starting server...");
        HelloWorldImpl implementor = new HelloWorldImpl();
        String address = "http://localhost:9000/hello";
        Endpoint.publish(address, implementor);
    }
}
