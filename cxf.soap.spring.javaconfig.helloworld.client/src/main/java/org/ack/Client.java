package org.ack;

import org.ack.configuration.SoapClientConfiguration;
import org.ack.service.HelloWorld;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Client {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SoapClientConfiguration.class);
        HelloWorld client = ctx.getBean(HelloWorld.class);
        System.out.println(client.sayHi("Arek"));
    }
}
