package org.ack.service;

import javax.jws.WebService;

@WebService(endpointInterface = "org.ack.service.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
    @Override
    public String sayHi(String text) {
        return "Hi " + text;
    }
}
