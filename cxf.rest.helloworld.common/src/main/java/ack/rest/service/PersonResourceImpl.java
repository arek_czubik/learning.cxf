package ack.rest.service;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonResourceImpl implements PersonResource {

    private Long currentId = 0L;
    private Map<Long, Person> people = new HashMap<>();

    public PersonResourceImpl() {
        this.add(new Person(1L, "Arek", 40));
        this.add(new Person(1L, "Asia", 35));
    }

    @Override
    public List<Person> getAll() {
        return people.values().stream().collect(Collectors.toList());
    }

    @Override
    public Person get(String id) {
        Long personId = Long.parseLong(id);
        return people.get(personId);
    }

    @Override
    public Response add(Person person) {
        person.setId(currentId++);
        people.put(person.getId(), person);

        return Response.ok(person).build();
    }

    @Override
    public void update(Person person) {

    }

    @Override
    public void delete(String id) {

    }
}
