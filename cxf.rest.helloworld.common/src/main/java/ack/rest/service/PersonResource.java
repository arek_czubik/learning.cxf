package ack.rest.service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface PersonResource {

    @Path("/people")
    @GET
    List<Person> getAll();

    @Path("/people/{id}")
    @GET
    Person get(@PathParam("id") String id);

    @Path("/people")
    @POST
    Response add(Person person);

    @Path("/people")
    @PUT
    void update(Person person);

    @Path("/people/{id}")
    @DELETE
    void delete(@PathParam("id") String id);
}
