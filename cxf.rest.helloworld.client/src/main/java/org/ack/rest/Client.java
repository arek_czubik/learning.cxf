package org.ack.rest;

import ack.rest.service.Person;
import ack.rest.service.PersonResource;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.cxf.binding.BindingFactoryManager;
import org.apache.cxf.jaxrs.JAXRSBindingFactory;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;

public class Client {

    public static void sampleWithJsonProvider() {
        JAXRSClientFactoryBean cf = new JAXRSClientFactoryBean();
        cf.setResourceClass(PersonResource.class);
        cf.setAddress("http://localhost:8080");
        cf.setProvider(new JacksonJsonProvider());

        BindingFactoryManager manager = cf.getBus().getExtension(BindingFactoryManager.class);
        JAXRSBindingFactory factory = new JAXRSBindingFactory();
        factory.setBus(cf.getBus());
        manager.registerBindingFactory(JAXRSBindingFactory.JAXRS_BINDING_ID, factory);
        PersonResource service = cf.create(PersonResource.class);
        WebClient wc = cf.createWebClient();

        Person person = wc.path("/people/1").accept("application/json").get(Person.class);
        System.out.println(person);
    }

    public static void simplestWebClientSampleNoJson() {
        WebClient client = WebClient.create("http://localhost:8080");
        Person person = client.path("/people/1").accept("application/xml").get(Person.class);
        System.out.println(person);
    }

    public static void yetAnotherClientSample() {
        PersonResource client = JAXRSClientFactory.create("http://localhost:8080/", PersonResource.class);
        WebClient.client(client).accept("application/xml");
        Person person = client.get("1");
        System.out.println(person);
    }

    public static void main(String[] args) {
        sampleWithJsonProvider();
        simplestWebClientSampleNoJson();
        yetAnotherClientSample();
    }
}
