package org.ack;

import ack.rest.service.PersonResourceImpl;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;

public class Server {
    public static void main(String[] args) {
        JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
        sf.setResourceClasses(PersonResourceImpl.class);
        sf.setResourceProvider(PersonResourceImpl.class, new SingletonResourceProvider(new PersonResourceImpl()));
        sf.setAddress("http://localhost:8080/");
        sf.setProvider(new JacksonJaxbJsonProvider());
        sf.create();
    }
}
